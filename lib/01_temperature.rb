def ftoc(num)
  (num - 32) * 5/9.to_f
end

def ctof(num)
  (num * (9/5.to_f)) + 32
end
