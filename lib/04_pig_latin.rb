def translate(sentence)
  nos = %w[a e i o]
  words = sentence.split
  words.map do |word|
    if !nos.include?(word[0])
      (word = word[1..-1] + word[0] until nos.include?(word[0]))
    else
      word
    end
    "#{word}ay"
  end.join(' ')
end
