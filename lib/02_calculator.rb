def add(num1, num2)
   num1 + num2
end

def subtract(num1, num2)
  num1 - num2
end

def sum(array)
  return 0 if array.empty?
  array.reduce (:+)
end

def multiply(array)
  array.reduce (:*)
end

def power(num1, num2)
  num1 ** num2
end

def factor(num)
  return 1 if num <= 1
  num * factor(num - 1)
end
