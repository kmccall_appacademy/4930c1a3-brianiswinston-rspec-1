def echo(word)
  word
end

def shout(word)
  word.upcase
end

def repeat(word, amount = 2)
  answer = []
    amount.times {answer << word}
  answer.join(" ")
end

def start_of_word(word, idx)
  word[0..idx - 1]
end

def first_word(sentence)
  sentence.split.first
end

def titleize(sentence)
  nos = %w(and over the)
  sentence.split.map.with_index {|word, idx| (idx == 0 || !nos.include?(word)) ? word.capitalize : word }.join(" ")
end
